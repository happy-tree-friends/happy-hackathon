const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);

var schema = {
	"type":"array",
	"minItems": 1,
	"maxItems": 1,
	"items":{
		type:'object',
		properties:{
				dollar1: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar2: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar3: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				tag1: {
					type: 'string',
					faker:'commerce.productAdjective'
					},
				tag2: {
					type: 'string',
					faker:'commerce.productAdjective'
					},
				tag3: {
					type: 'string',
					faker:'commerce.productAdjective'
					},
				tag4: {
					type: 'string',
					faker:'commerce.productAdjective'
					},
				tag5: {
					type: 'string',
					faker:'commerce.productAdjective'
					},
				tag6: {
					type: 'string',
					faker:'commerce.productAdjective'
					}
		},
		required:['dollar1','dollar2','dollar3','tag1','tag2','tag3','tag4','tag5','tag6',]

	}
};



/* GET users listing. */
router.get('/', (req, res) => {

	jsf.resolve(schema).then(sample => {
		res.send(sample);
	});
});

module.exports = router;




