const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);

var schema = {
	"type":"array",
	"minItems": 1,
	"maxItems": 1,
	"items":{
		type:'object',
		properties:{
			dollar1: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar2: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar3: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar4: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar5: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar6: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar7: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar8: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},
				dollar9: {
					type: 'integer',
					"minimum": 1,
					"maximum": 35
					},


    			status1: {

  					"type": "string",
  					"pattern": "available|unavailable"

					},
				status2: {
					"type": "string",
  					"pattern": "available|unavailable"
					},
				status3: {
					"type": "string",
  					"pattern": "available|unavailable"
					}
					
		},
		required:['dollar1','dollar2','dollar3','dollar4','dollar5','dollar6','dollar7','dollar8','dollar9','status1','status2','status3']

	}
};



/* GET users listing. */
router.get('/', (req, res) => {

	jsf.resolve(schema).then(sample => {
		res.send(sample);
	});
});

module.exports = router;

/*
  {"name": faker.name.firstName(), 
   "statue":faker.random.boolean(), 
   "age":chance.age({type:'teen'}),
   "stoimost":chance.dollar()
   }
			
 */
